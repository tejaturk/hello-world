---
title: "My First Post"
date: 2021-05-15T13:16:12+02:00
draft: false
---

## Welcome to my blog!

> _When a flower doesn’t bloom, you fix the environment in which it grows, not the flower._ &ndash; Alexander Den Heijer

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

#### Markdown

```r
> 1 + 1
[1] 2
```

Simply write your blog using **Markdown** syntax.

  * a
  * b
  * c
    1. X
    2. Y
