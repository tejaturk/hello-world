# Hello, world!

This is the GitLab repository accompanying the Hello, world! blog post at [www.tejaturk.com/posts/hello_world/](https://www.tejaturk.com/posts/hello_world/). See the blog post for details about this repository.

Link to the resulting website: [tejaturk.gitlab.io/hello-world/](https://tejaturk.gitlab.io/hello-world/)
